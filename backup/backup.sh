#!/bin/sh

# Default value
: "${REFRESH_INTERVAL:=1d}"

MONGO_HOST="mongodb://mongoread:mongopass@mongo/CN"

SSH_COMMAND="timeout 2h $SSH_COMMAND"

BACKUP_DIR="/mnt/eseesnrd90_TASCI_manexec/Backup"
$SSH_COMMAND mkdir -p $BACKUP_DIR

while true
do
  echo "Sleep until new run"
  sleep "$REFRESH_INTERVAL"
  echo

  DATE=$(date +%Y-%m-%d)

  # Check if it is Sunday
  if [ "$(date +%u)" -eq 7 ]
  then
    SUFFIX=weekly
  else
    SUFFIX=daily
  fi
  RESULT=OK

  echo "Back up CN Results MongoDB"
  if mongodump "$MONGO_HOST" --db CN --gzip --archive=temporary
  then
    scp temporary "root@$SSH_LOGS:$BACKUP_DIR/cn-results-$DATE-$SUFFIX.archive.gz"
  else
    RESULT=NOK
  fi

  for JENKINS in $SSH_JENKINS_MASTER $SSH_JENKINS_VNF
  do
    echo "Back up $JENKINS"
    # For modern Jenkins versions, you can simplify the below command with --exclude="workspace"
    if $SSH_COMMAND ssh "root@$JENKINS" tar --totals -C /var/lib/jenkins --exclude="**/workspace*" --exclude="**/builds" -czf temporary .
    then
      $SSH_COMMAND scp "root@$JENKINS:temporary" "$BACKUP_DIR/$JENKINS-$DATE-$SUFFIX.tar.gz"
    else
      RESULT=NOK
    fi
  done

  echo "Remove older backups"
  if [ "$RESULT" = "OK" ]
  then
    $SSH_COMMAND find "$BACKUP_DIR" -name "*daily*" -mtime +3 -print -exec rm {} +
    $SSH_COMMAND find "$BACKUP_DIR" -name "*weekly*" -mtime +40 -print -exec rm {} +
  fi
done
