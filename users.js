db = db.getSiblingDB("CN");
db.createUser(
  {
    user: "mongoread",
    pwd: "mongopass",
    roles: [ { role: "read", db: "CN" } ]
  }
);
db.createUser(
  {
    user: "mongowrite",
    pwd: _getEnv("MONGO_INITDB_ROOT_PASSWORD"),
    roles: [ { role: "readWrite", db: "CN" } ]
  }
);
