# Jenkins Results

✔️ Collects all Robot Framework test cases execution results stored in
all the job folders for a given Jenkins host

✔️ Stores them in MongoDB

✔️ Presents them via Mongo Express front-end

## Limitations

 - The jobs in question should be inside a jobs folder

 - The results are collected every one minute

 - Apart from the URLs of the Jenkins jobs, the crawler records

    * the time that each job started

    * its duration in minutes

    * the description of the job run, recorded in the `note` field
 
 - The values of the following parameters are collected too

    * `BRANCH`

    * `HASH`

    * `EXECUTION_TYPE`

    * `PROFILE`

       + The string of the last parameter is converted into upper case

 - If `HASH` is `latest`, then the Console Output of the job is fetched

    * `latest` is substituted by the relevant `[INFO]: The latest hash is` line

## Execution

 - Clone the project

 - Inside its folder create a `.env` file containing e.g.
   ```
   JENKINS_HOST=http://localhost:8080
   ```

 - If you have Docker Compose installed, execute
   ```
   docker compose up -d
   ```

 - Navigate with your browser to `http://localhost`

 - Use the following user interface to search for a specific branch name

   ![Find window](search.png)

 - Select `Regex` in the last field, as in the above figure

## MongoDB

 - MongoDB is exposed via the default 27017 port

 - `mongoread` user is able only to read it using the `mongopass` password

 - `mongowrite` is the administrator user with a password set in the `.env` file as
   ```
   MONGO_PASSWORD=type_your_password_here
   ```
