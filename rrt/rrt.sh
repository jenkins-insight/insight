#!/bin/sh
# shellcheck disable=SC2016

# Default value
: "${REFRESH_INTERVAL:=1m}"

MONGO_HOST="mongodb://mongowrite:$MONGO_PASSWORD@mongo/CN"

echo "Wait for MongoDB initialization"
until mongoexport --uri "$MONGO_HOST" -c RRT > /dev/null
do
  sleep 1
done

SSH_COMMAND="timeout 2h $SSH_COMMAND"

$SSH_COMMAND mkdir -p "/mnt/eseesnrd90_TASCI_manexec/RRT" "/mnt/eseesnrd90_TASCI_manexec/Recycle"

while true
do
  echo "Fetch Jenkins data"
  if curl -m 60 -gsSf \
    "$JENKINS_VNF_HOST/api/json?tree=jobs[jobs[jobs[name,fullDisplayName,lastCompletedBuild[timestamp,duration,url]]]]" > \
    jenkins.json
  then
    echo "Process JSON"
    if jq -c '[ .jobs[].jobs[]?.jobs[] |
      select(.fullDisplayName | test("^branch-(master|ntas-\\d+-\\d+)?(_?mq-\\S*)? ")) |
      select(.name == "51-Deploy-And-FT" and .lastCompletedBuild != null) | {

      _id: null,
      branch: .fullDisplayName,
      passed: null,
      failed: null,
      started: (.lastCompletedBuild.timestamp / 1000) | strftime("%Y-%m-%d %H:%M"),
      hours: (.lastCompletedBuild.duration / 3600000) | round,
      logs: .lastCompletedBuild.url

      } ] | sort_by(.started)[]' jenkins.json > all.json
    then
      while read -r JOB_RUN
      do
        BRANCH=$(echo "$JOB_RUN" | jq -r .branch | sed -e 's/branch-//' -e 's/ .*//')
        DATE=$(echo "$JOB_RUN" | jq -r .started)
        URL=$(echo "$JOB_RUN" | jq -r .logs)

        echo
        echo "Check for existing entry for $BRANCH $DATE"
        mongoexport --uri "$MONGO_HOST" -c RRT --query \
          '{"branch": "'"$BRANCH"'", "started": "'"$DATE"'"}' > existing.json
        if [ ! -s existing.json ]
        then
          echo "Fetch console output"
          curl -m 60 -gsSf "$URL/consoleText" > console

          HASH=$(grep "GIT_COMMIT_HASH" console | grep -o "[0-9a-f]*" | head -c 8)
          LOGS_DIR=$(grep "Log drive link" console | grep -o "[^ ]*$" | sed 's~.*/TAS_EVO/~~')
          PASSED=$(grep -c "^|PASS" console)
          FAILED=$(grep -c "^|FAIL" console)

          if [ -z "$HASH" ] || [ -z "$LOGS_DIR" ] || [ "$PASSED" -eq 0 ]
          then
            continue
          fi

          echo "Remove previous logs for the same branch"
          mongoexport --uri "$MONGO_HOST" -c RRT --query '{"branch": "'"$BRANCH"'",
                                                           "logs": {"$ne": ""}}' |
            jq --slurp -c '.[] .logs = "" | .[]' |
            mongoimport --uri "$MONGO_HOST" -c RRT --mode upsert

          if ! $SSH_COMMAND rm -rf "/mnt/eseesnrd90_TASCI_manexec/RRT/$BRANCH" &&
             ! $SSH_COMMAND mv "/mnt/eseesnrd90_TASCI_manexec/RRT/$BRANCH" "/mnt/eseesnrd90_TASCI_manexec/Recycle"
          then
            echo "Unexpected ssh/rm error: Going to restart..."
            sleep "$REFRESH_INTERVAL"
            exit 1
          fi

          if echo "$BRANCH" | grep -Eq "^mq-|_mq-"
          then
            echo "Remove logs for the pre-previous branch that targets the same release"
            PREFIX=$(echo "$BRANCH" | sed 's/mq-.*//')
            mongoexport --uri "$MONGO_HOST" -c RRT --query '{"branch": {"$regex":
              "^'"$PREFIX"'mq-"}, "logs": {"$ne": ""}}' --sort '{started:-1}' --skip 1 |
            while read -r DOC
            do
              OLD_BRANCH=$(echo "$DOC" | jq -r .branch)
              echo "$DOC" | jq '.logs = ""' |
                mongoimport --uri "$MONGO_HOST" -c RRT --mode upsert

              if ! $SSH_COMMAND rm -rf "/mnt/eseesnrd90_TASCI_manexec/RRT/$OLD_BRANCH" &&
                 ! $SSH_COMMAND mv "/mnt/eseesnrd90_TASCI_manexec/RRT/$OLD_BRANCH" "/mnt/eseesnrd90_TASCI_manexec/Recycle"
              then
                echo "Unexpected ssh/rm error: Going to restart..."
                sleep "$REFRESH_INTERVAL"
                exit 1
              fi
            done
          fi

          echo "Insert the document for the new logs"
          if $SSH_COMMAND cp -r "/mnt/eseesnrd90_TASCI_manexec/TAS_EVO/$LOGS_DIR" \
                                "/mnt/eseesnrd90_TASCI_manexec/RRT/$BRANCH" 2> error
          then
            LOGS="http://$LOGS_HOST/RRT/$BRANCH"
          else
            cat error
            if grep -q " stat " error
            then
              LOGS=""
            else
              echo "Unexpected ssh/cp error: Going to restart..."
              sleep "$REFRESH_INTERVAL"
              exit 1
            fi
          fi

          echo "$JOB_RUN" | jq '._id    = "'"$HASH"'"   |
                                .branch = "'"$BRANCH"'" |
                                .passed =  '"$PASSED"'  |
                                .failed =  '"$FAILED"'  |
                                .logs   = "'"$LOGS"'"' |
            mongoimport --uri "$MONGO_HOST" -c RRT --mode upsert
        fi
      done < all.json
    fi
  fi
  echo "Sleep until new run"
  sleep "$REFRESH_INTERVAL"
done
